package com.demo.xbus;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Process;
import android.view.View;
import android.widget.TextView;

import com.open.xbus.XBus;

/**
 * 服务端，注册函数
 */
public class XBusActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_root);
        // 注册需要调用的函数.
        XBus.getInstance().register(ITestData.class, TestData.class);
        //
        TextView tv = findViewById(R.id.pid_tv);
        tv.setText("进程PID：" + Process.myPid());
        findViewById(R.id.act2_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(XBusActivity.this, BActivity.class));
            }
        });
    }

}
