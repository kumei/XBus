package com.demo.xbus;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Process;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.open.xbus.XBus;
import com.open.xbus.XBusService;

public class BActivity extends Activity {

    int mIsCallBack = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_root);
        XBus.getInstance().init(this);
//        XBus.getInstance().init(this, XBusService.NewXBusService.class);
//        XBus.getInstance().init(this, "com.demo.xbus");
        //
        final Drawable drawable = new TestDrawable();

        final Bitmap bitmap = Bitmap.createBitmap(100, 100, Bitmap.Config.ARGB_8888);
        //
        TextView tv = findViewById(R.id.pid_tv);
        tv.setText("进程PID：" + Process.myPid());
        findViewById(R.id.root_llyt).setBackgroundColor(Color.YELLOW);
        final Button btn = findViewById(R.id.act2_btn);
        btn.setText("点击调用TestData的函数");
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ITestData testData = XBus.getInstance().getCreateCall(ITestData.class);
                String result = "";
                switch (mIsCallBack) {
                    case 0: // 正常的函数调用
                        result = testData.testtesttest(10, "正常函数");
                        mIsCallBack++;
                        break;
                    case 1: // 使用一些自定义类的调用
                        result = testData.testtesttest(520, "带其它参数的", new OtherMode());
                        mIsCallBack++;
                        break;
                    case 2: // 带回调的测试.
                        result = testData.testtesttest(780, "回调函数", new ITestCallBack() {
                            @Override
                            public void callback(int progress) {
                                Toast.makeText(BActivity.this, "返回回调数字:" + progress, Toast.LENGTH_LONG).show();
                            }
                        });
                        mIsCallBack++;
                        break;
                    case 3: // 带 drawable, bitmap 的测试
                        result = testData.testBitmapAndDrawable(2000, "回调函数", drawable, bitmap, null);
                        mIsCallBack = 0;
                        break;
                }
                btn.setText("drawable:" + drawable + " bitmap:" + bitmap + "\n返回值:" + result);
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        XBus.getInstance().disconnect(this);
    }

}
