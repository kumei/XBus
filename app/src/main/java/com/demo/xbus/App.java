package com.demo.xbus;

import android.app.Application;

import com.open.xbus.XBus;

public class App extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        // 注册需要调用的函数.
        XBus.getInstance().register(ITestData.class, TestData.class);
    }

}
