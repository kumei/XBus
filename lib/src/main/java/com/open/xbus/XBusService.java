package com.open.xbus;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

import static com.open.xbus.XBus.TAG;

public class XBusService extends Service {

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "XBusService onCreate");
    }

    @Override
    public IBinder onBind(Intent intent) {
        Log.d(TAG, "XBusService onBind");
        return new XBusAidlImpl();
    }

    public static class NewXBusService extends XBusService {
    }

}
