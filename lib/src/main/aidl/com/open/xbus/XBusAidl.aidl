// XBusAidl.aidl
package com.open.xbus;

import com.open.xbus.Response;
import com.open.xbus.Request;
import com.open.xbus.IXbusCallback;

interface XBusAidl {

    Response run(in Request request);
    void register(IXBusCallback cb, int pid);
}
